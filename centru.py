import openpyxl
import os

#importing stuffimport os
def cls():
    os.system('cls' if os.name=='nt' else 'clear')
path = 'data/lista_02_04.xlsx'
wb = openpyxl.load_workbook(path)

#sheet to variable
c1 = wb['C1']
c2 = wb['C2']

#declaring variables
running = True
class Exiting(Exception): pass
cls()

date = 'A'
name = 'C'
cnp7 = 'D'
phone = 'E'
status = 'H'
observation = 'I'


#defining functions

def save():
    try:
        wb.save(path)
    except PermissionError:
        print("Unable to write to file." + str(PermissionError))


def retrieve_name(cnp):
    for cell in c1[cnp7]: #iterate over cnp cells in C1
        if cnp == cell.value:
            c1[status + '{}'.format(cell.row)] = 'X' # marking down patient
            print(c1[name + '{}'.format(cell.row)].value + ' | Centrul 1' + ' | ' + str(c1[status + '{}'.format(cell.row)].value))
            save()
    for cell in c2[cnp7]: #iterate over cnp cells in C2 
        if cnp == cell.value:
            c2[status + '{}'.format(cell.row)] = 'X' # marking down patient
            print(c2[name + '{}'.format(cell.row)].value + ' | Centrul 2' + ' | ' + str(c2[status + '{}'.format(cell.row)].value)) # display pacient status
            save()       
def execute_functions(function):
    if function == 'close':
        running = False
        return 1
    if function == 'save':
        save()
        running = False
        return 1
    if function == 'clear':
        cnp = input('Insert cnp to clear: ')
        for cell in c1[cnp7]: #iterate over cnp cells in C1
            if cnp == cell.value:
                c1[status + '{}'.format(cell.row)] = None # clear patient
                print(c1[name +'{}'.format(cell.row)].value + ' | Centrul 1' + ' | ' + str(c1[status + '{}'.format(cell.row)].value))
                save()
        for cell in c2['D']: #iterate over cnp cells in C2
            if int(cnp) == cell.value:
                c2[status + '{}'.format(cell.row)] = None # clear patient
                print(c2[name + '{}'.format(cell.row)].value + ' | Centrul 2' + ' | ' + str(c2[status + '{}'.format(cell.row)].value))  
                save()
    
    if function == 'left':
        print('-----CENTRUL 1 -----')
        count = 0
        for cell in c1[status]:
            if cell.value is None:
                count = count + 1
                print('{} | {} | {} '.format(str(c1[date + '{}'.format(cell.row)].value),str(c1[name + '{}'.format(cell.row)].value),str(c1[phone + '{}'.format(cell.row)].value)))
        print (count)
        print('-----CENTRUL 2 -----')
        count = 0
        for cell in c2['H']:
            if cell.value is None:
                count = count + 1
                print('{} | {} | {} '.format(str(c1[date + '{}'.format(cell.row)].value),str(c1[name + '{}'.format(cell.row)].value),str(c2[phone + '{}'.format(cell.row)].value)))
        print(count)

    if function == 'hour':
        hour = input('Enter the hour: ')
        cellhour = '2021-04-01 {}:00:00'.format(str(hour))
        print('-----CENTRUL 1 -----')
        for cell in c1[date]:
            if cell.value == cellhour:
                print('{} | {} | {} | {}'.format(str(c1[date + '{}'.format(cell.row)].value),str(c1[name + '{}'.format(cell.row)].value),str(c1[phone + '{}'.format(cell.row)].value),str(c1[status + '{}'.format(cell.row)].value)))
        print('-----CENTRUL 2 -----')
        for cell in c2['A']:
            if cell.value == cellhour:
                print('{} | {} | {} | {}'.format(str(c2[date + '{}'.format(cell.row)].value),str(c2[name + '{}'.format(cell.row)].value),str(c2[phone + '{}'.format(cell.row)].value),str(c2[status + '{}'.format(cell.row)].value)))

    if function == 'obs':
        cnp = input('Insert cnp for obs: ')
        for cell in c1[cnp7]: #iterate over cnp cells in C1
            if cnp == cell.value:
                obs = input('Insert obs: ')
                c1[observation + '{}'.format(cell.row)] = obs
                print(c1[name + '{}'.format(cell.row)].value + ' | Centrul 1' + ' | ' + str(c1[observation + '{}'.format(cell.row)].value))
                save()
        for cell in c2[cnp7]: #iterate over cnp cells in C2
            if int(cnp) == cell.value:
                obs = input('Insert obs: ')
                c2[observations + '{}'.format(cell.row)] = obs
                print(c2[name + '{}'.format(cell.row)].value + ' | Centrul 2' + ' | ' + str(c2[observation + '{}'.format(cell.row)].value))  
                save()



#main loop
while running == True:
    inp = input("Introduceti 7 cifre din CNP: ")
    try:
        retrieve_name(int(inp))
    except:
        if execute_functions(inp) == 1:
            break